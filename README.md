## universis-auth
### Universis Experimental OAuth2 Server

#### Installation prerequisites

**1. Install node.js** 

Node.js v8.11.1 LTS must be installed either using the installation media provided by [node.js](https://nodejs.org/en/)
-for Windows or Mac OSX operating systems- or using [the package manager for linux distibutions](https://nodejs.org/en/download/package-manager/)

**2. Install Ngnix or apache web server (Optional)**

A frontend web server must installed and used as a proxy server for any node.js application which is a part of student portal ecosystem.
This web server may be the [nginx web server](https://nginx.org/en/), the [Apache HTTP server](https://httpd.apache.org/) or any other server
that may be used as a standard application proxy.

**_Nginx web server installation_**

[How to install Nginx on Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-16-04)

[How to install Nginx on CentOS 7](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-centos-7)

[How to install Nginx on Windows](https://vexxhost.com/resources/tutorials/nginx-windows-how-to-install/)


**_Apache web server 2.4 installation_**

[How to install the Apache Web Server on Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-install-the-apache-web-server-on-ubuntu-16-04)

[How to install Apache Web Server on CentOS 7](https://www.linode.com/docs/web-servers/apache/install-and-configure-apache-on-centos-7/)

[How to install Apache Web Server on Windows](https://httpd.apache.org/docs/2.4/platform/windows.html)

**3. Install PM2 Process Manager (Optional)**

[PM2 Advanced Process Manager for node.js](http://pm2.keymetrics.io/) must be installed by following the installation instructions 
provided in [PM2 quick start article](http://pm2.keymetrics.io/docs/usage/quick-start/)

    npm install pm2@latest -g 
    
[How to install pm2 as service on Linux Distributions](http://pm2.keymetrics.io/docs/usage/startup/)

[How to install pm2 as service on Windows](https://www.npmjs.com/package/pm2-windows-service)

#### Installation

**1. Checkout source code**

Checkout source code by executing the following command

    git clone https://gitlab.com/universis/universis-auth.git

Go to project directory:

    cd universis-auth
    
**2. Install dependencies**

Install application dependencies by executing the following command:

    npm i
    
**3. Configure application**

Read "Configuration" section below

**4. Build from source code**

Build source code by executing the following command:

    npm run build
    
**5. Start application**

You can start application by executing:

    npm start

Open your browser and navigate to http://localhost:3000

Note: Before start application, please read the configuration section below.

`universis-auth` repository has an SQLite local database under db/local.db for development purposes. Read more about this database [here](db/readme.md)

    
**6. Register PM2 Process (these step is optional and it is required only if you are intended to use PM2 process manager)**

Create configuration file for pm2.config.json

    {
        "name"        : "universis_auth",
        "script"      : "./bin/www",
        "watch"       : true,
        "env": {
          "IP": "0.0.0.0"
        }
    }

and set the binding IP address in environment variable "IP".

Register PM2 process by executing:

    pm2 start pm2.config.json

### Docker container installation

Navigate to application root directory and build docker image:

    # build docker image
    docker build . -t universis-auth:latest
    
Create docker container

    docker run -d \
      --name=auth \
      --mount source=auth,destination=/usr/src/app/src/config \
      universis-auth:latest

Run docker container console

    docker exec -it auth /bin/bash
    
Get list of client applications

    npm run cli list clients
    
        ...
        client_id         name                       client_secret                     redirect_uri                         grant_type
        ----------------  -------------------------  --------------------------------  -----------------------------------  ----------
        7444341691607290  OAuth2 Server Application  ufpk6pbcUS9AnuUUUAmxAghXQQdHvpor  http://localhost:3000/auth/callback  code      
        ...               ...                        ...                               ...                                  code,token

Exit from docker container

    exit
    
Edit src/config/app.production.json in docker mount volume /var/lib/docker/volumes/auth/_data/
and set client_id and client_secret of OAuth2 Server Application

    ...
    "auth": {
        ...
        "name": ".AUTH",
        "timeout": 480,
        "slidingExpiration": false,
        "loginPage":"/login",
        "client_id":".....",
        "client_secret":"...."
    },
    ...

Restart container

    docker restart auth

Note: Read "Configuration" section below for further information about application configuration

#### Configuration

Application configuration base settings are defined in src/config/app.json file.
Copy src/config/app.json as app.production.json

    cp src/config/app.json src/config/app.production.json
    
or

    copy src\config\app.json src\config\app.production.json

on Windows.

Open production configuration app.production.json:

    {
        "handlers": [
            { "name":"language", "type":"/handlers/language-handler" },
            { "name":"basic-auth", "type":"@themost/web/handlers/noop" },
            { "name":"node_modules", "type":"@themost/web/handlers/node-modules" }
        ],
        "engines": [
            { "name": "EJS View Engine", "extension": "ejs", "type": "@themost/web/engines/ejs" },
            { "name": "Markdown View Engine", "extension": "md", "type": "@themost/web/engines/md" }
        ],
        "settings": {
            "app": {
                "title": "Student Information System | Authentication Services",
                "shortTitle": "SIS Authentication Services"
            },
            "localization":{
                "cultures":["en-us","el-gr"],
                "default":"el-gr"
            },
            "crypto": {
                "algorithm": "aes256",
                "key": "684b6b6652417277345175704543614a515236465559586b51444d43424d4b765248384d4144457273433939783466636b7a48754b714e704353423737753863396f52716d427667"
            },
            "auth": {
                "unattendedExecutionAccount":"DKuHN2MJLwuFznD7sz",
                "name": ".AUTH",
                "timeout": 480,
                "slidingExpiration": false,
                "loginPage":"/login",
                "client_id":"4519411990612752",
                "client_secret":"N8BEVgvvQFxLkDk7BpuUXUhNSuTMvkMw"
            },
            "mail": {
                "service": "gmail",
                "from":"services@example.com",
                "auth": {
                    "user": "services@example.com",
                    "pass": "pass"
                }
            }
        },
        "adapterTypes": [
            { "name":"SQLite Data Adapter", "invariantName": "sqlite", "type":"@themost/sqlite" },
            { "name":"MySQL Data Adapter", "invariantName": "mysql", "type":"@themost/mysql" },
            { "name":"MSSQL Server Data Adapter", "invariantName": "msssql", "type":"@themost/mssql" }
        ],
        "adapters": [
            {
                "name": "local-db", "invariantName": "sqlite", "default": true,
                "options": {
                    "database": "./db/local.db"
                }
            },
            {
                "name": "mysql-db", "invariantName": "mysql", "default": true,
                "options": {
                    "host":"localhost",
                    "port":3306,
                    "user":"root",
                    "password":"pass",
                    "database":"members_dev"
                }
            },
            {
                "name": "mssql-db", "invariantName": "mssql", "default": false,
                "options": {
                    "server":"localhost",
                    "user":"user",
                    "password":"pass",
                    "database":"members_dev"
                }
            }
        ],
        "mimes": [
            { "extension": ".css", "type": "text/css" },
            { "extension": ".js", "type": "application/javascript" },
            { "extension": ".json", "type": "application/json", "encoding": "utf-8" },
            { "extension": ".xml", "type": "text/xml" },
            { "extension": ".rdf", "type": "text/xml" },
            { "extension": ".xsd", "type": "text/xml" },
            { "extension": ".less", "type": "text/css" },
            { "extension": ".png", "type": "image/png" },
            { "extension": ".ico", "type": "image/x-icon"},
            { "extension": ".gif", "type": "image/gif"},
            { "extension": ".bmp", "type": "image/bmp"},
            { "extension": ".jpg", "type": "image/jpeg" },
            { "extension": ".jpeg", "type": "image/jpeg"},
            { "extension": ".html", "type": "text/html" },
            { "extension": ".htm", "type": "text/html" },
            { "extension": ".pdf", "type": "application/pdf" },
            { "extension": ".txt", "type": "text/plain"},
            { "extension": ".svg", "type": "image/svg+xml"},
            { "extension": ".woff", "type": "application/x-font-woff"},
            { "extension": ".woff2", "type": "application/x-font-woff"},
            { "extension": ".ttf", "type": "application/octet-stream" },
            { "extension": ".map", "type": "application/json" },
            { "extension": ".csv","type": "application/csv" }
        ]
    }


**settings.app**

Defines application general settings as title and short title.

     "app": {
         "title": "Student Information System | Authentication Services",
         "shortTitle": "SIS Authentication Services"
     }


**settings.mail**

This section describes the settings that are going to be used while sending notification mails. 
The default settings for connecting with an SMTP server are:

    "mail": {
        "host": "smtp.example.com",
        "port": 587,
        "secure": false,
        "auth": {
            "user": "username",
            "pass": "password"
        }
    }

If you are going to used a known email service like Gmail use the following settings:

    {
      "service": "gmail",
      "from":"username@gamil.com",
      "auth": {
        "user": "username@gmail.com",
        "pass": "password"
      }
    }

**settings.localization**

This section describes the localization settings for this application.

_settings.localization.culture_

Sets an array of the available cultures like ["en-us","el-gr"]

_settings.localization.default_

Sets the default culture of this application

**settings.crypto**

This section describes the cryptographic algorithm and the cryptographic key which are going to used by
 MOST Web Framework DefaultEncyptionStrategy service. 
 The cryptographic key is a random hex string and it is important to generate a new one for every application instance.
 The key length must be 32, 48, 64 or 128.
 
    "crypto": {
              "algorithm": "aes256",
              "key": "615542466f6439436b413858645656766237356b7054644268777a6f4471703665416d435166754b714a4e4634655243"
            }
 
 Note: You can use MOST Web Framework CLI to generate a new key. Install CLI globally by executing the following command:
 
    npm i @themost/cli -g
    
 and execute
 
    themost random hex --length 48 
    
**settings.auth**

This section defines general authentication settings.

    "auth": {
                "unattendedExecutionAccount":"aozCT84MCnvbgSfuQ5X2fB",
                "name": ".AUTH2",
                "timeout": 60,
                "loginPage":"/login",
                "client_id":"1111127691648401",
                "client_secret":"qpWoCsUxbSCWk5gzT4JeKTk6XQnuzomv"
            }

_settings.auth.unattendedExecutionAccount_

Sets the unattended execution account for internal application processes. It is better to avoid using a valid application account. Define a random string

Note: You can use MOST Web Framework CLI to generate a random. Install CLI globally by executing the following command:
 
    npm i @themost/cli -g
    
 and execute
 
    themost random string --length 16 

_settings.auth.name_

Defines the name of the authentication HTTP cookie e.g. .AUTH2

_settings.auth.timeout_

Defines the cookie lifetime in minutes e.g. 60 

_settings.auth.loginPage_

Defines the login page relative URL 

_settings.auth.client_id_

Defines the client id of OAuth2 Server application 

_settings.auth.client_secret_

Defines the client secret of OAuth2 Server application 

Important Note: Execute

    npm run cli list clients
    
to get client_id and client_secret of OAuth2 Server Application

    ...
    client_id         name                       client_secret                     redirect_uri                         grant_type
    ----------------  -------------------------  --------------------------------  -----------------------------------  ----------
    7444341691607290  OAuth2 Server Application  ufpk6pbcUS9AnuUUUAmxAghXQQdHvpor  http://localhost:3000/auth/callback  code      
    ...               ...                        ...                               ...                                  code,token

Universis auth server generates by default new clients for the core applications of Universis project.

**adapterTypes**

Defines an array of available MOST Web Framework adapter types for this application. These adapter types are going to be used for connection with application data storage. 

    "adapterTypes": [
            { "name":"SQLite Data Adapter", "invariantName": "sqlite", "type":"@themost/sqlite" },
            { "name":"MySQL Data Adapter", "invariantName": "mysql", "type":"@themost/mysql" },
            { "name":"MSSQL Server Data Adapter", "invariantName": "msssql", "type":"@themost/mssql" }
          ]

Note: MOST Web Framework Generic Pool Adapter may be used for controlling connection pooling.

**adapters**

This section contains information about connecting with data storages used by this application.

Important Note: You can set the default adapter by setting adapter.default to true 

    "adapters": [
            {
                "name": "mysql-db", "invariantName": "mysql", "default": true,
                "options": {
                    "host":"localhost",
                    "port":3306,
                    "user":"root",
                    "password":"pass",
                    "database":"members_dev"
                }
            }
          ]

If you are intended to use generic pool adapter add a new item in adapters' collection, define the name of the base adapter and set the pool size:

      { "name":"mysql-db-pool", "invariantName":"pool", "default":false,
          "options": {
              "adapter":"mysql-db",
              "size": 25
          }
      }

#### OAuth2 Server Command Line Utility

Usage: npm run cli -- [options]

Commands:

    cli add <command>  Add a new object (client, user, scope etc)
       
    cli add client 
       
       Adds a new client application
       
       Options:
         --version  Show version number                                       [boolean]
         --help     Show help                                                 [boolean]
         --dev      Enables development mode                           [default: false]
         --name     Sets client application name e.g. Test Application
                                       [default: "http://localhost:8080/auth/callback"]
         --uri      Sets client application callback URI e.g.
                    https://client.example.com/auth/callback
                                       [default: "http://localhost:8080/auth/callback"]
         --grant    Sets the client grant types represented by a comma separated string
                    e.g. code,token or code,password etc              [default: "code"]
         --scope    Sets the client scopes represented by a comma separated string e.g.
                    profile or profile,user,user:read etc          [default: "profile"]
       
    cli add scope   
       
       Adds or updates an application scope
       
       Options:
         --version      Show version number                                   [boolean]
         --help         Show help                                             [boolean]
         --dev          Enables development mode                       [default: false]
         --name         Sets scope name e.g. profile:read          [default: "profile"]
         --uri          Sets scope URI e.g. https://auth.example.com/scopes/profile
         --description  Sets scope description e.g. View profile data

    cli add user
       
       Adds or updates a user
       
       Options:
         --version      Show version number                                   [boolean]
         --help         Show help                                             [boolean]
         --dev          Enables development mode                       [default: false]
         --name         Sets user name e.g. user@example.com
                                                          [default: "user@example.com"]
         --description  Sets user description e.g. John Doe
         --password     Sets user password                             [default: false]

       
    cli list clients
       
        Lists all client applications
   
       Options:
         --version  Show version number                                       [boolean]
         --help     Show help                                                 [boolean]
         --dev      enables development mode                           [default: false]
         --filter   defines query filter                                [default: null]
         --top      defines query top option                              [default: 25]
         --skip     defines query skip option                              [default: 0]

    cli list scopes
    
        Lists all authorization scopes
    
        Options:
          --version  Show version number                                       [boolean]
          --help     Show help                                                 [boolean]
          --dev      enables development mode                           [default: false]

    cli list users
    
        Lists users
        
        Options:
          --version  Show version number                                       [boolean]
          --help     Show help                                                 [boolean]
          --dev      enables development mode                           [default: false]
          --filter   defines query filter                                [default: null]
          --top      defines query top option                              [default: 25]
          --skip     defines query skip option                              [default: 0]


    cli drop <command>
    
    Drops a client application
    
    cli drop client <client_id>
    
        Drops a client application
        
        Options:
          --version  Show version number                                       [boolean]
          --help     Show help                                                 [boolean]
          --dev      Enables development mode                           [default: false]

**Important Note**: If you are going to add options e.g. list only the first 10 clients we should pass arguments with a double dash before e.g.

    npm run cli list clients -- --top=10
    
The special option -- is used by getopt to delimit the end of the options. npm will pass all the arguments after the -- directly to cli script.
       

#### Nginx Configuration

If you are using Nginx as web server create a configuration file for Student Portal Local Auth Server (auth.conf) and place it in nginx configuration directory:

    server {
        listen 80;
        server_name auth.example.com;
        # redirects both www and non-www to ssl port with http (NOT HTTPS, forcing error 497)
        return 301 https://auth.example.com/;
    }
    
    server {
        listen              443 ssl;
        server_name         auth.example.com;
        ssl_certificate     /etc/nginx/ssl/server.bundled.crt;
        ssl_certificate_key /etc/nginx/ssl/server.key;
            location / {
                    proxy_pass http://127.0.0.1:3000;
                    proxy_http_version 1.1;
                    proxy_set_header Upgrade $http_upgrade;
                    proxy_set_header Connection "upgrade";
            }
    }
    
Important Note: The default port of Universis OAuth2 Server is 3000. If you want to change this port 
you need to define PORT environment variable in application startup script (See PM2 Process Manager documentation on how to define environment variables)


#### Apache Configuration

If you are using Apache as web server enable apache proxy modules

[How To Use Apache HTTP Server As Reverse-Proxy Using mod_proxy Extension](https://www.digitalocean.com/community/tutorials/how-to-use-apache-http-server-as-reverse-proxy-using-mod_proxy-extension)


Create a configuration file for Student Portal Local Auth Server (auth.conf) and place it in apache configuration directory:

    <VirtualHost *:80>
    
      ServerName auth.example.com
      ServerAlias auth.example.com
      RewriteEngine On 
      RewriteCond %{HTTPS} !=on 
      RewriteRule ^/?(.*) https://%{SERVER_NAME}/$1 [R,L]
      
    </VirtualHost>
    
    <VirtualHost *:443>
    
      SSLEngine on
      ServerName auth.example.com
      ServerAlias auth.example.com
      SSLCertificateFile "/etc/apache2/conf/ssl/server.crt"
      SSLCertificateKeyFile "/etc/apache2/conf/ssl/server.key"
      SSLCACertificateFile "/etc/apache2/conf/ssl/server-ca-bundled.crt"
    
      ProxyPass / http://127.0.0.1:3000/
      ProxyPassReverse /  http://127.0.0.1:3000/
      
    </VirtualHost>

#### Integrate SIS OAuth2 Server with an existing user infrastructure

If you are intended to integrate OAuth2 server with an existing user infrastructure complete the following steps:

*1. Define User model data source*

Important Note: User model must be set to sealed (by setting DataModel.sealed attribute in config/models/User.json schema) in order to prevent automatic model upgrade

*2. Define Group model data source*

Important Note: Group model must be set to sealed (by setting DataModel.sealed attribute in config/models/Group.json schema) in order to prevent automatic model upgrade

*3. Define UserCredential model data source*

Important Note: UserCredential model must be set to sealed (by setting DataModel.sealed attribute in config/models/UserCredential.json schema) in order to prevent automatic model upgrade

*4. Set database adapter connection properties*
