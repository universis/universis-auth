##Welcome

Use your **academic account** to login to Student Information Services.

Student Information System allows **students** to:
- view courses, semester registrations and grades
- apply for semester registration
- apply for academic certificates

Also offers a wide set of features to **instructors** for:
- viewing courses, classes and students
- managing active classes and course examinations
- uploading digitally signed grades for course examinations