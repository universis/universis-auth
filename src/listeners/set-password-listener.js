
import { DataObjectState } from "@themost/data";
/**
 * @param {DataEventArgs} event 
 */
async function afterSaveAsync(event) {
    if (event.state === DataObjectState.Insert || event.state === DataObjectState.Update) {
        if (Object.prototype.hasOwnProperty.call(event.target, 'userCredentials')) {
            if (event.target.userCredentials.userPassword != null) {
                const context = event.model.context;
                await context.model('UserCredential').insert({
                    id: event.target.id,
                    userPassword: event.target.userCredentials.userPassword,
                    userActivated: true
                });
            }
        }
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
 export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}