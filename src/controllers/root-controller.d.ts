import { HttpBaseController, HttpViewResult } from "@themost/web";

declare class RootController extends HttpBaseController {
    getWelcome(): HttpViewResult;
    getLogout(): Promise<HttpRedirectResult>;
    getIndex(): HttpViewResult;
    getLogin(client_id: string, response_type: string, 
        redirect_uri: string, scope: string): Promise<HttpViewResult>;
    postLogin(client_id: string, redirect_uri: string, 
        response_type: string, credentials: any, scope: string): Promise<any>;    
}

export  = RootController;