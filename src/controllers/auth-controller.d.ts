import { HttpBaseController } from "@themost/web";

declare class AuthController extends HttpBaseController {
    getMe(access_token: string): Promise<any>;
    postTokenInfo(access_token: string): Promise<any>;
    getAccessToken(client_id: string, client_secret: string, code: string): Promise<any>;
    getAuthorize(client_id: string, redirect_uri: string, response_type: string, scope: string): Promise<any>;
    postAuthorize(grant_type: string, client_id: string, client_secret: string, 
        scope: string, username: string, password: string): Promise<any>;
}
export = AuthController;