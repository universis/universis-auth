import { HttpBadRequestError, HttpNotFoundError, HttpForbiddenError } from '@themost/common';
/**
 * @class
 * @augments AuthenticateRequestHandler
 */
export class BearerAuthHandler {
    static createInstance() {
        return new BearerAuthHandler();
    }
    authenticateRequest(context, callback) {
        if (context.request && context.request.headers && Object.prototype.hasOwnProperty.call(context.request.headers, 'authorization')) {
            /**
             * @type {string}
             */
            const authorizationHeader = context.request.headers['authorization'];
            if (authorizationHeader.startsWith('Bearer ')) {
                // get token
                const bearerToken = authorizationHeader.substr(authorizationHeader.lastIndexOf(' ') + 1);
                // todo: validate token
                return context.model('AccessToken').where('access_token').equal(bearerToken).silent().getTypedItem().then((token) => {
                    if (token == null) {
                        // todo: throw error
                        return callback(new HttpBadRequestError('Token was expired.'));
                    }
                    return token.is(':expired').then((expired) => {
                        if (expired) {
                            return callback(new HttpNotFoundError('The specified token was not found.'));
                        }
                        // set user
                        return context.model('User').where('name').equal(token.user_id).silent().getItem().then((user) => {
                            if (user == null) {
                                return callback(new HttpBadRequestError('User profile was not found.'));
                            }
                            if (user.enabled === false) {
                                return callback(new HttpForbiddenError('Account is disabled.'));
                            }
                            Object.assign(context, {
                                user: {
                                    name: user.name,
                                    authenticationProviderKey: user.id,
                                    authenticationType: 'Bearer',
                                    authenticationToken: token.access_token,
                                    authenticationScope: token.scope
                                }
                            });
                            return callback();
                        });
                    });
                }).catch((err) => {
                    return callback(err);
                });
            }
        }
        return callback();
    }
}

/**
 * @returns {BearerAuthHandler}
 */
 export function createInstance() {
    return new BearerAuthHandler();
}