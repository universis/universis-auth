import { DataError } from "@themost/common"

export declare class OutdatedDataError extends DataError {
    constructor(code?: string, message?: string, innerMessage?: string);
}

export declare class IdentityServerError extends DataError {
    constructor(code?: string, message?: string, innerMessage?: string);
}

export declare class ValidateCredentialsError extends DataError {
    constructor(code?: string, message?: string, innerMessage?: string);
}

export declare class UnknownUsernameOrPasswordError extends DataError {
    constructor(username: string, code?: string, message?: string, innerMessage?: string);
}

export declare class LoginServerError extends DataError {
    constructor(username: string, code?: string, message?: string, innerMessage?: string);
}

export declare class InvalidDataError extends DataError {
    constructor(code?: string, message?: string, innerMessage?: string);
}