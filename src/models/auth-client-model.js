
import {DataObject} from '@themost/data/data-object';
import {EdmMapping} from '@themost/data/odata';
import _ from 'lodash';
/**
 * @property {string} client_id
 * @property {string} name
 * @property {string} client_secret
 * @property {string} grant_type
 * @property {string} redirect_uri
 * @property {Array<*>} scopes
 */
@EdmMapping.entityType('AuthClient')
class AuthClient extends DataObject {
    constructor() {
        super();
    }

    /**
     * @param {string} grant_type
     * @returns {boolean}
     */
    hasGrantType(grant_type) {
        if (_.isNil(this.grant_type)) {
            return false;
        }
        return (this.grant_type.split(',').indexOf(grant_type)>=0);
    }

    /**
     * @param {string} scope
     * @returns {Promise<boolean>}
     */
    async hasScope(scope) {
        if (typeof scope !== 'string') {
            throw new TypeError('Invalid argument. Scope must be a string');
        }
        // split scopes
        let scopes = scope.split(',');
        if (Array.isArray(this.scopes)) {
            let found = scopes.filter(scope => {
                return this.scopes.findIndex( x=> {
                   return new RegExp('^' + scope + '$','i').test(x.name);
                })>=0;
            }).length;
            return found === scopes.length;
        }
        // count scopes
        let count = await this.property('scopes').where('name').equal(scopes).silent().count();
        // count must be equal to scopes length
        return count === scopes.length;
    }

    /**
     * @param {string} redirect_uri
     * @returns {boolean}
     */
    hasRedirectUri(redirect_uri) {
        // get collection of redirect_uri
        if (_.isString(redirect_uri)) {
            let re;
            let result;
            if (Array.isArray(this.redirect_uris)) {
                for (let index = 0; index < this.redirect_uris.length; index++) {
                    const element = this.redirect_uris[index];
                    re = new RegExp('^' + element.replace("*","(.*?)") + '$','ig');
                    result = re.test(redirect_uri);
                    if (result) {
                        return result;
                    }
                }
            }
            if (typeof this.redirect_uri === 'string') {
                re = new RegExp('^' + this.redirect_uri.replace("*","(.*?)") + '$','ig');
                return re.test(redirect_uri);
            }
        }
        return false;
    }

}

module.exports = AuthClient;