import { DataObject } from "@themost/data";
declare class AuthClient extends DataObject {
    hasGrantType(grant_type: string): boolean;
    hasScope(scope: string): Promise<boolean>;
    hasRedirectUri(redirect_uri: string): boolean;
}
export = AuthClient;