import {EdmMapping,EdmType} from '@themost/data';
import Account = require('./account-model');
import Group = require('./group-model');

/**
 * @class
 */
declare class User extends Account {

     public id?: number; 
     
     public lockoutTime?: Date; 
     
     public logonCount?: number; 
     
     public enabled: boolean; 
     
     /**
      * @description The last time and date the user logged on.
      */
     public lastLogon?: Date; 
     
     /**
      * @description A collection of groups where user belongs.
      */
     public groups?: Array<Group|any>;

}

export = User;