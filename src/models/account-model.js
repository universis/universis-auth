import {EdmMapping} from '@themost/data';
let Thing = require('./thing-model');

@EdmMapping.entityType('Account')
class Account extends Thing {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = Account;