import {DataObject, EdmMapping} from "@themost/data";

@EdmMapping.entityType('Group')
class Group extends DataObject {
    constructor() {
        super();
    }
}

module.exports = Group;