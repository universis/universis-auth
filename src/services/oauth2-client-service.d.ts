import { ApplicationService } from '@themost/common';


export declare interface AuthorizeUser {
    client_id?: string;
    client_secret?: string;
    username: string;
    password: string;
    grant_type: string;
    scope?: string;
}

export declare interface AdminMethodOptions {
    access_token?: string;
}

export declare class OAuth2ClientService extends ApplicationService {
    constructor(app: ApplicationBase)

    getProfile(context: DataContext, token: string): Promise<User | any>;
    getTokenInfo(context: DataContext, token: string): Promise<any>;
    getContextTokenInfo(context: DataContext): Promise<any>;
    authorize(authorizeUser: AuthorizeUser): Promise<{ access_token?: string, refresh_token?: string}>;
    getUser(username: string, options: AdminMethodOptions): Promise<any>;
    getUserById(user_id: any, options: AdminMethodOptions): Promise<any>;
    getUserByEmail(email: string, options: AdminMethodOptions): Promise<any>;
    updateUser(user: any, options: AdminMethodOptions): Promise<any>;
    createUser(user: any, options: AdminMethodOptions): Promise<any>;
    deleteUser(user: { id: any }, options: AdminMethodOptions): Promise<any>; 

}
