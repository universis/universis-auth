import { ConfigurationStrategy } from '@themost/common';

export declare interface ScopeAccessElement {
    scope: Array<string>;
    resource: string;
    access: Array<string>;
}

export declare class ScopeAccessConfiguration extends ConfigurationStrategy {
    get elements(): Array<ScopeAccessElement>;
    constructor(app: ConfigurationStrategy)
    verify(req): Promise<any>;
}

export declare class DefaultScopeAccessConfiguration extends ScopeAccessConfiguration {

}
