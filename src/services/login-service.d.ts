import { HttpContext } from "@themost/web";

export declare class LoginService {
    constructor(context: HttpContext, client_id?: string, scope?: string);
    getContext(): HttpContext;
    setClient(client_id: string): this;
    getClient(): string;
    getScope(): string;
    login(userName: string, userPassword: string, callback?: (err?: any, result?: any) => void);
    logout(): Promise<void>;
}

export declare class InvalidClientError extends Error {
    constructor(msg: string);
}

export declare class InvalidScopeError extends Error {
    constructor(msg: string);
}

export declare class InvalidCredentialsError extends Error {
    constructor(msg: string);
}