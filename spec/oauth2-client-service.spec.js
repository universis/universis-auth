import {OAuth2ClientService} from '../src';
// eslint-disable-next-line no-unused-vars
import {HttpApplication} from '@themost/web';
import express from 'express';
import http from 'http';
// eslint-disable-next-line no-unused-vars
import {TextUtils, TraceUtils} from '@themost/common';
import * as request from 'supertest'
/**
 * @type {HttpApplication}
 */

describe('OAuthClientService', () => {

    let container;
    let server_uri;
    beforeAll((done) => {
        // init http application
        const application = require('../src/server');
        // init application container
        container = express();
        container.set(HttpApplication.name, application);
        container.use(application.runtime());
        // serve application
        let server = http.createServer(container);
        server.on('listening', () => {
            let addr = server.address();
            // eslint-disable-next-line no-console
            server_uri = `http://localhost:${addr.port}`;
            return done();
        });
        server.listen();

    });

    it('should create instance', async () => {
        const app = container.get(HttpApplication.name);
        const service  = new OAuth2ClientService(app);
        expect(service).toBeTruthy();
    });

    it('should authorize user', async () => {
        const app = container.get(HttpApplication.name);
        const service  = new OAuth2ClientService(app);
        Object.assign(service.settings, {
            server_uri: server_uri
        });
        const result = await service.authorize({
            client_id: '4671239296469335',
            client_secret: 'fxssW5UnrhmuUrrwm7FLT27YAuFpgQxs',
            username: 'registrar1@example.com',
            password: '918875B6F3FD',
            grant_type: 'password',
            scope: 'registrar'
        });
        expect(result.access_token).toBeTruthy();

        await expectAsync(service.authorize({
            client_id: '4671239296469335',
            client_secret: 'fxssW5UnrhmuUrrwm7FLT27YAuFpgQxs',
            username: 'registrar1@example.com',
            password: '--pass',
            grant_type: 'password',
            scope: 'registrar'
        })).toBeRejected()
        
    });


    it('should get token info', async () => {
        const app = container.get(HttpApplication.name);
        const service  = new OAuth2ClientService(app);
        Object.assign(service.settings, {
            server_uri: server_uri
        });
        const result = await service.authorize({
            client_id: '4671239296469335',
            client_secret: 'fxssW5UnrhmuUrrwm7FLT27YAuFpgQxs',
            username: 'registrar1@example.com',
            password: '918875B6F3FD',
            grant_type: 'password',
            scope: 'registrar'
        });
        expect(result.access_token).toBeTruthy();

        const context = {
            user: {
                name: 'registrar1@example.com'
            }
        }
        const info = await service.getTokenInfo(context, result.access_token);
        expect(info).toBeTruthy();
        expect(info.active).toBe(true);
        expect(info.username).toBe('registrar1@example.com');
        
    });

    it('should get context token info', async () => {
        const app = container.get(HttpApplication.name);
        const service  = new OAuth2ClientService(app);
        Object.assign(service.settings, {
            server_uri: server_uri
        });
        const result = await service.authorize({
            client_id: '4671239296469335',
            client_secret: 'fxssW5UnrhmuUrrwm7FLT27YAuFpgQxs',
            username: 'registrar1@example.com',
            password: '918875B6F3FD',
            grant_type: 'password',
            scope: 'registrar'
        });
        expect(result.access_token).toBeTruthy();

        let context = {
            user: {
                name: 'registrar1@example.com',
                authenticationType: 'Bearer',
                authenticationToken: result.access_token
            }
        }
        const info = await service.getContextTokenInfo(context);
        expect(info).toBeTruthy();
        expect(info.active).toBe(true);
        expect(info.username).toBe('registrar1@example.com');
        
    });


    it('should get user', async () => {
        const app = container.get(HttpApplication.name);
        const service  = new OAuth2ClientService(app);
        Object.assign(service.settings, {
            server_uri: server_uri,
            admin_uri: new URL('/api/', server_uri).toString()
        });
        const result = await service.authorize({
            client_id: '4671239296469335',
            client_secret: 'fxssW5UnrhmuUrrwm7FLT27YAuFpgQxs',
            username: 'registrar1@example.com',
            password: '918875B6F3FD',
            grant_type: 'password',
            scope: 'admin'
        });
        expect(result.access_token).toBeTruthy();
        let user = await service.getUser('registrar1@example.com', {
            access_token: result.access_token
        });
        expect(user).toBeTruthy();
        user = await service.getUserById(user.id, {
            access_token: result.access_token
        });
        expect(user).toBeTruthy();
    });

    it('should create user', async () => {
        const app = container.get(HttpApplication.name);
        const service  = new OAuth2ClientService(app);
        Object.assign(service.settings, {
            server_uri: server_uri,
            admin_uri: new URL('/api/', server_uri).toString()
        });
        const result = await service.authorize({
            client_id: '4671239296469335',
            client_secret: 'fxssW5UnrhmuUrrwm7FLT27YAuFpgQxs',
            username: 'registrar1@example.com',
            password: '918875B6F3FD',
            grant_type: 'password',
            scope: 'admin'
        });
        await service.createUser({
            name: 'user1@example.com',
            enabled: true
        }, {
            access_token: result.access_token
        });
        let user = await service.getUser('user1@example.com', {
            access_token: result.access_token
        });
        expect(user).toBeTruthy();
        expect(user.name).toBe('user1@example.com');
        await service.deleteUser(user, {
            access_token: result.access_token
        });
    });

    it('should create user with password', async () => {
        const app = container.get(HttpApplication.name);
        const service  = new OAuth2ClientService(app);
        Object.assign(service.settings, {
            server_uri: server_uri,
            admin_uri: new URL('/api/', server_uri).toString()
        });
        const result = await service.authorize({
            client_id: '4671239296469335',
            client_secret: 'fxssW5UnrhmuUrrwm7FLT27YAuFpgQxs',
            username: 'registrar1@example.com',
            password: '918875B6F3FD',
            grant_type: 'password',
            scope: 'admin'
        });
        await service.createUser({
            name: 'user1@example.com',
            enabled: true,
            userCredentials: {
                userPassword: `{md5}${TextUtils.toMD5('secret')}`
            }
        }, {
            access_token: result.access_token
        });
        let user = await service.getUser('user1@example.com', {
            access_token: result.access_token
        });
        expect(user).toBeTruthy();
        expect(user.name).toBe('user1@example.com');

        // login with new user
        let newToken = await service.authorize({
            client_id: '4671239296469335',
            client_secret: 'fxssW5UnrhmuUrrwm7FLT27YAuFpgQxs',
            username: 'user1@example.com',
            password: 'secret',
            grant_type: 'password',
            scope: 'students'
        });
        expect(newToken).toBeTruthy();

        await service.deleteUser(user, {
            access_token: result.access_token
        });
    });
});