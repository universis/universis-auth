## Local database for development

`universis-auth` repository has an SQLite local database under db/local.db for development purposes. This sample database is the default database as it has been defined in `src/config/app.json`.

     "adapters": [
        {
            "name": "local-db", "invariantName": "sqlite", "default": true,
            "options": {
                "database": "./db/local.db"
            }
        }
     ]

There are a set of sample clients for core applications and services of [universis](https://gitlab.com/universis):

    [
        {
            "client_id": "4671239296469335",
            "name": "Universis Development OAuth2 Server",
            "client_secret": "fxssW5UnrhmuUrrwm7FLT27YAuFpgQxs",
            "redirect_uri": "http://localhost:3000/auth/callback",
            "grant_type": "code,password"
        },
        {
            "client_id": "6065706863394382",
            "name": "Universis Students Application",
            "client_secret": "chwHTCKkMD72dUrLU5zqhBwzud28Fmsr",
            "redirect_uri": "http://localhost:7001/*",
            "grant_type": "token"
        },
        {
            "client_id": "3693655920803557",
            "name": "Universis Teachers Application",
            "client_secret": "UQrSYUTC4KRdASexJe2VkomCNczcAs6o",
            "redirect_uri": "http://localhost:7002/*",
            "grant_type": "token"
        },
        {
            "client_id": "4362743690216159",
            "name": "Universis Registrar Application",
            "client_secret": "uUVxuXLmRDM7FWSA8Be5LSUNLrNxaEaU",
            "redirect_uri": "http://localhost:4200/*",
            "grant_type": "token"
        },
        {
            "client_id": "1914729668211931",
            "name": "Universis Development Api Server",
            "client_secret": "URbS5MBfU5SYX8EXUQDM7xQZaKcuYR77",
            "redirect_uri": "http://localhost:5001/auth/callback",
            "grant_type": "code"
        }
    ]

These clients may be used for developing universis applications like [universis-students](https://gitlab.com/universis/universis-students), [universis-teachers](https://gitlab.com/universis/universis-teachers), [universis registrar application](https://gitlab.com/universis/universis) etc.

Configure your client application to use one of these clients by following instructions provided by each application
Here is a part of configuration of `universis-students`.

    "auth": {
            "authorizeURL": "http://localhost:3000/authorize",
            "logoutURL": "http://localhost:3000/logout?continue=http://localhost:7001/#/auth/login",
            "userProfileURL": "http://localhost:5001/users/me",
            "oauth2": {
                "tokenURL": "http://localhost:3000/tokeninfo",
                "clientID": "6065706863394382",
                "callbackURL": "http://localhost:7001/auth/callback/index.html",
                "scope": [
                    "students"
                ]
            }
        }

Sample database contains also a set of users for testing purposes:

    [
        {
            "name": "student1@example.com",
            "description": "Student1",
            "userPassword": "7B18D8EADF81"
        },
        {
            "name": "student2@example.com",
            "description": "Student2",
            "userPassword": "0BCC0826443E"
        },
        {
            "name": "student3@example.com",
            "description": "Student3",
            "userPassword": "BBC3D55E8A1E"
        },
        {
            "name": "teacher1@example.com",
            "description": "Teacher1",
             "userPassword": "E8F6410918FF"
        },
        {
            "name": "teacher2@example.com",
            "description": "Teacher2",
             "userPassword": "552D2ED8C352"
        },
        {
            "name": "teacher3@example.com",
            "description": "Teacher3",
             "userPassword": "7B2FB575C01E"
        },
        {
            "name": "registrar1@example.com",
            "description": "Registrar1",
             "userPassword": "918875B6F3FD"
        }
    ]

    